*** Settings ***
Suite Setup       Start Tests
Suite Teardown    End Tests
Resource          ../base.robot
Resource          ../Artifacts/tests_keywords.robot
Resource          ../Artifacts/tests_variables.robot


*** Test Cases ***

Add Book
    Wait and Click  ${Global.Home}
    wait and click  ${Global.Dolacz}
    wait and click  ${Global.Button_register_login}
    input text  ${Global.Email_input}   ${username}
    input text  ${Global.Password_input}    ${password}
    wait and click  ${Global.Button_login}
    wait and click  ${Global.Ksiazki}
    wait until element is visible   //ion-title[contains(text(),'Ksi')]
    wait and click  ${Global.AddBook}
    ${randomnum}=     Generate Random String  2   [NUMBERS])
    ${isbn}=     Generate Random String  5   [NUMBERS])
    wait until element is visible   //ion-label[contains(text(),'Tytuł')]/..//input
    input text  //ion-label[contains(text(),'Tytuł')]/..//input     test${randomnum}
    input text  //ion-label[contains(text(),'ISBN')]/..//input    ${isbn}
    input text  //ion-label[contains(text(),'Ilość stron')]/..//input     ${randomnum}
    input text  //ion-textarea//textarea   loremipsum
    wait and click  //ion-button[contains(text(),'Dodaj')]
    wait until element is visible   ${Global.Error}