*** Settings ***
Suite Setup       Start Tests
Suite Teardown    End Tests
Resource          ../base.robot
Resource          ../Artifacts/tests_keywords.robot
Resource          ../Artifacts/tests_variables.robot


*** Test Cases ***

Admin Login
    wait and click  ${Global.Home}
    wait and click  ${Global.Dolacz}
    wait and click  ${Global.Button_register_login}
    input text  ${Global.Email_input}   ${username}
    input text  ${Global.Password_input}    ${password}
    wait and click  ${Global.Button_login}
    wait until element is visible   ${Global.Search}
    wait until element is visible   ${Global.MojeKsiazki}
    wait until element is visible   ${Global.Api}
    wait until element is visible   ${Global.Ksiazki}