*** Settings ***
Resource          ../base.robot
Resource          ../Artifacts/tests_variables.robot

*** Keywords ***

Start Tests
    # Suite Setup
    set selenium timeout  15 seconds
    ${now}    Get Time    epoch
    set global variable  ${now}
    ${chrome options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    ${prefs}    Create Dictionary    download.default_directory=${tempdir}
    Call Method    ${chrome options}    add_experimental_option    prefs    ${prefs}
    Create Webdriver    ${BROWSER}    chrome_options=${chrome options}
    go to    ${URL}
    Maximize Browser Window


End Tests
    # Suite Teardown
    Close All Browsers

Wait and Click
    [Arguments]    ${arg}
    Wait Until Page Contains Element  ${arg}  timeout=10s
    Click Element    ${arg}

Refresh page
    Reload Page

Press escape
    Press Keys  None  '\ue00c'

